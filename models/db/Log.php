<?php

namespace app\models\db;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%log}}".
 *
 * @property int $id
 * @property int $type
 * @property string|null $message
 * @property string|null $created_at
 *
 * @property $typeToString
 */
class Log extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%log}}';
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    const TYPE_LOW_TRACE = 1;
    const TYPE_HIGH_TRACE = 2;
    const TYPE_LOW_INFO = 3;
    const TYPE_HIGH_INFO = 4;
    const TYPE_LOW_WARNING = 5;
    const TYPE_HIGH_WARNING = 6;
    const TYPE_LOW_ERROR = 7;
    const TYPE_HIGH_ERROR = 8;
    const TYPE_LOW_FATAL = 9;
    const TYPE_HIGH_FATAL = 10;

    /**
     * All types of logs
     *
     * @return string[]
     */
    public static function getLevels()
    {
        return [
            self::TYPE_LOW_TRACE => 'Краткая трассировка',
            self::TYPE_HIGH_TRACE => 'Подробная трассировка',
            self::TYPE_LOW_INFO => 'Краткая информация',
            self::TYPE_HIGH_INFO => 'Подробная информация',
            self::TYPE_LOW_WARNING => 'Краткое предупреждение',
            self::TYPE_HIGH_WARNING => 'Подробное предупреждение',
            self::TYPE_LOW_ERROR => 'Краткая ошибка',
            self::TYPE_HIGH_ERROR => 'Подробная ошибка',
            self::TYPE_LOW_FATAL => 'Краткое фаталити',
            self::TYPE_HIGH_FATAL => 'Подробное фаталити',
        ];
    }

    /**
     * Return log type as String
     *
     * @return string|null
     */
    public function getTypeToString()
    {
        return key_exists($this->type, $levels = self::getLevels())
            ? $levels[$this->type]
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'integer'],
            [['message'], 'string', 'max' => 255],
            [['created_at'], 'datetime'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'message' => 'Сообщение',
            'created_at' => 'Дата создания',
        ];
    }
}
