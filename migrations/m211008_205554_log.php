<?php

use yii\db\Migration;

/**
 * Class m211008_205554_log
 */
class m211008_205554_log extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \yii\db\Exception
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $this->createTable('{{%log}}', [
            'id'            => $this->primaryKey(10)->unsigned(),
            'type'          => $this->integer(1)->notNull(),
            'message'       => $this->string(255),
            'created_at'    => $this->dateTime(),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->createIndex('index-log-type', '{{%log}}', 'type');
        $this->createIndex('index-log-created_at', '{{%log}}', 'created_at');

        $time = time();

        for($i = 0; $i < 10000; $i++) {

            $columns = [
                'type' => mt_rand(1, 10),
                'message' => json_encode([
                    'key' => Yii::$app->security->generateRandomString(10),
                    'count' => mt_rand(100, 999),
                    'time' => $time
                ], JSON_UNESCAPED_UNICODE),
                'created_at' => gmdate('Y-m-d H:i:s', $time - $i * mt_rand(10, 10000))
            ];

            Yii::$app->db->createCommand()->insert('{{%log}}', $columns)->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%log}}');
    }
}
