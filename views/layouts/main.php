<?php

/* @var $content    string */
/* @var $this       View */

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Breadcrumbs;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?=Yii::$app->charset?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?=Html::csrfMetaTags()?>
        <title><?=Html::encode($this->title)?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid label-primary">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?=Yii::$app->homeUrl?>">
                        <img src="<?=Yii::getAlias("@web/img/logo.png")?>" alt="" height="100%">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#">
                                Link <span class="sr-only">(current)</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="wrap">
            <div class="container-fluid">
                <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? []])?>
            </div>
            <div class="content">
                <div class="container">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 text-center">
                        &copy; <?=Yii::$app->name?> <?=date('Y')?>
                    </div>
                    <div class="col-xs-6 text-center">
                        <strong><?=''?></strong>
                    </div>
                </div>
            </div>
        </footer>
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>