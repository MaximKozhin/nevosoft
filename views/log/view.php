<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\db\Log */

?>
<div class="log-view">
    <div class="panel panel-default">
        <div class="panel-body">
            <h1 class="no-margin clearfix">
                <?= Html::encode($this->title) ?>
            </h1>
            <hr>
            <div class="grid-view">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'type',
                            'value' => $model->typeToString
                        ],
                        'message',
                        [
                            'attribute' => 'created_at',
                            'format' => ['datetime', 'php:Y-m-d H:i:s'],
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
