<?php

use app\models\db\Log;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Log */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="log-index">
    <div class="panel panel-default">
        <div class="panel-body">
            <h1 class="no-margin clearfix"><?= Html::encode($this->title) ?></h1>
            <hr>
            <div class="grid-view">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        #['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['width' => 60]
                        ],
                        [
                            'attribute' => 'type',
                            'format' => 'html',
                            'headerOptions' => ['width' => 200],
                            'filter' => Log::getLevels(),
                            'value' => function($model) {
                                return $model->typeToString;
                            },
                        ],
                        'message',
                        [
                            'attribute' => 'created_at',
                            'format' => ['datetime', 'php:Y-m-d H:i:s'],
                            'headerOptions' => ['width' => 200],
                        ],

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
